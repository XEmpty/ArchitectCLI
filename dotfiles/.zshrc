# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt INC_APPEND_HISTORY_TIME

export GTK_MODULES=appmenu-gtk-module
export SAL_USE_VCLPLUGIN=gtk

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Source manjaro-zsh-configuration
if [[ -e ~/.zsh-extra-config ]]; then
  source ~/.zsh-extra-config
fi

source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

ZSH=/usr/share/oh-my-zsh/

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

PATH="$HOME/.node_modules/bin:$PATH"
export npm_config_prefix=~/.node_modules
export NODE_PATH=`npm config get prefix`/lib/node_modules/:$NODE_PATH

PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"
alias la='colorls -lA --sd -X'
alias ls='colorls --sd -X'
