# ArchLinux CLI Installer - XEmpty

Archlinux Preinstall & Postinstall Installer Script from terminal. [Fork - Helmuthdu/aui]

Before start try : `VMWARE` or `VBOX`

What u need : internet connection & login as `root`

## Download

- `mount -o remount,size=2G /run/archiso/cowspace`
- `pacman -Sy git`
- `git clone git://gitlab.com/XEmpty/ArchitectCLI`

### How to use
  
- PreInstall : `cd <repo_name> && ./preinstall`
- PostInstall : `cd <repo_name> && ./postinstall`

### Roadmap (Important Things)

- Full Support : Wine, ESync, DXVK, Lutris. 
- Full Support : Web Enviroment & Other Languages (Less / More).
- Full Support : Plasma & Openbox (QT Apps).

- Better Hardware Detect.
- Add Manual Settings : (View, Edit, Add, Remove, Set).
- Rewrite All Functions and CLI Looks. 

- Default Fonts, Wallpapers, Configs ( Applications Settings, CPU & GPU Configs (Optional) ) 
- Support Only Best Applications (Free, Commercial). 

  Note : Latte-Dock might slow system (in my case it is.)
         Animation working smooth without latte.


